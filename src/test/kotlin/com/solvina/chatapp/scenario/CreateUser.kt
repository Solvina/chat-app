package com.solvina.chatapp.scenario

import com.ninja_squad.dbsetup.DbSetup
import com.ninja_squad.dbsetup.Operations
import com.ninja_squad.dbsetup.destination.DataSourceDestination
import com.solvina.chatapp.data.repository.UserRepository
import com.solvina.chatapp.dbsetup.TestDatabaseSetup
import junit.framework.TestCase.*
import org.junit.Before
import org.junit.Ignore
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.RequestBuilder
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.model
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import javax.sql.DataSource


/**
 * User: Vlastimil
 * Date: 1/22/18
 * Time: 9:08 AM
 *
 *
 */
@AutoConfigureMockMvc
@SpringBootTest
@RunWith(SpringJUnit4ClassRunner::class)
class CreateUser {


    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    lateinit var mockMvc: MockMvc
    @Autowired
    lateinit var passwordEncoder: PasswordEncoder

    @Autowired
    @Qualifier("dataSource")
    lateinit var dataSource: DataSource

    val testEmail = "test@user.com"
    val testName = "test user name"
    val testPassword = "pass"

    var newUserRequest: RequestBuilder = post("/addNewUser")
            .param("email", testEmail)
            .param("password", "pass")
            .param("note", "test user for me")
            .param("name", testName)
            .with(csrf())

    @Before
    fun setUp() {

        DbSetup(
                DataSourceDestination(dataSource),
                Operations.sequenceOf(TestDatabaseSetup.DELETE_ALL)
        ).launch()
        val userBefore = userRepository.findByEmail(testEmail)
        assertNull(userBefore)

    }


    @Test
    fun shouldCreateNewUser() {
        mockMvc.perform(newUserRequest).andExpect(status().isOk);

        val userAfter = userRepository.findByEmail(testEmail)!!
        assertEquals(userAfter.email, testEmail)
        assertEquals(userAfter.name, testName)
        assertEquals(userAfter.password, passwordEncoder.encode(testPassword))

    }

    @Test
    @Ignore("Not working")
    fun shouldRejectTwoSimilarUsers() {
        var userBefore = userRepository.findByEmail(testEmail)
        assertNull(userBefore)

        mockMvc.perform(newUserRequest).andExpect(status().isOk);

        //TODO: Investigate the error handling, customize expected error statuses
        mockMvc.perform(newUserRequest).andExpect(model().hasErrors())
                .andExpect(model().errorCount(1))
    }
}
