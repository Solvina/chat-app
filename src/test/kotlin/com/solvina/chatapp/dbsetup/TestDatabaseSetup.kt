package com.solvina.chatapp.dbsetup

import com.ninja_squad.dbsetup.Operations
import com.ninja_squad.dbsetup.operation.Operation


/**
 * User: Vlastimil
 * Date: 12/7/17
 * Time: 2:09 PM
 *
 *
 */
object TestDatabaseSetup {
    val DEFAULT_STAMP = "2017-11-12 08:00:01"


    val DELETE_ALL: Operation = Operations.deleteAllFrom("article", "role", "user_group", "user_channel", "channel", "group_table", "user")
    val INSERT_USER_ROLE: Operation = Operations.sequenceOf(
            Operations.insertInto("user")
                    .columns("user_id", "email", "name", "password", "stamp")
                    .values(1, "test@test.com", "John", "none", DEFAULT_STAMP)
                    .values(2, "test2@test.com", "John", "none", DEFAULT_STAMP)
                    .values(3, "test3@test.com", "David", "none", DEFAULT_STAMP)
                    .build(),
            Operations.insertInto("role")
                    .columns("email", "role", "stamp")
                    .values("test@test.com", "USER", DEFAULT_STAMP)
                    .values("test2@test.com", "USER", DEFAULT_STAMP)
                    .values("test3@test.com", "USER", DEFAULT_STAMP)
                    .build()
    )
    val INSERT_GROUPS: Operation = Operations.insertInto("group_table")
            .columns("group_id", "name", "stamp")
            .values("1", "alpha", DEFAULT_STAMP)
            .values("2", "beta", DEFAULT_STAMP)
            .values("3", "gamma", DEFAULT_STAMP)
            .build()

    val INSERT_GROUPS_USERS = Operations.insertInto("user_group")
            .columns("user_id", "group_id")
            .values("1", "1")
            .values("1", "2")
            .values("1", "3")
            .values("2", "1")
            .values("2", "2")
            .values("3", "2")
            .values("3", "3")
            .build()
    val INSERT_CHANNELS: Operation = Operations.insertInto("channel")
            .columns("channel_id", "name", "group_id", "user_id", "stamp")
            .values("1", "public alpha", "1", "1", DEFAULT_STAMP)
            .values("2", "public beta", "2", "2", DEFAULT_STAMP)
            .values("3", "public gamma", "3", "3", DEFAULT_STAMP)
            .values("4", "new channel in alpha", "1", "1", DEFAULT_STAMP)
            .build()

    val INSERT_USERS_CHANNELS: Operation = Operations.insertInto("user_channel")
            .columns("user_id", "channel_id")
            .values("1", "1")
            .values("1", "2")
            .values("1", "3")
            .values("1", "4")
            .values("2", "1")
            .values("2", "2")
            .values("2", "4")
            .values("3", "2")
            .values("3", "3")
            .build()

    val INSERT_ARTICLES: Operation = Operations.insertInto("article")
            .columns("article_id", "user_id", "channel_id", "text", "stamp")
            .values("1", "1", "1", "first test in public alpha channel", DEFAULT_STAMP)
            .values("2", "2", "1", "answer in public alpha channel", DEFAULT_STAMP)
            .values("3", "2", "2", "text in beta channel", DEFAULT_STAMP)
            .values("4", "3", "2", "another text beta channel", DEFAULT_STAMP)
            .values("5", "1", "3", "text in gamma", DEFAULT_STAMP)
            .build()

    val FULL_SETUP: Operation = Operations.sequenceOf(DELETE_ALL,
            INSERT_USER_ROLE,
            INSERT_GROUPS,
            INSERT_GROUPS_USERS,
            INSERT_CHANNELS,
            INSERT_USERS_CHANNELS,
            INSERT_ARTICLES)
}


