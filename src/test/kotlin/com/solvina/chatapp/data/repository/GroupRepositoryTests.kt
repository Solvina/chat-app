package com.solvina.chatapp.data.repository

import com.ninja_squad.dbsetup.DbSetup
import com.ninja_squad.dbsetup.destination.DataSourceDestination
import com.solvina.chatapp.dbsetup.TestDatabaseSetup
import mu.KLogging
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.sql.DataSource


/**
 * User: Vlastimil
 * Date: 12/13/17
 * Time: 9:20 AM
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner::class)
@DataJpaTest
class GroupRepositoryTests {
    companion object : KLogging()

    @Autowired
    lateinit var groupRepository: GroupRepository
    @Autowired
    lateinit var userRepository:UserRepository

    val email = "test@test.com"

    @Autowired
    @Qualifier("dataSource")
    lateinit var dataSource: DataSource

    @Before
    fun setUp() {
        DbSetup(DataSourceDestination(dataSource), TestDatabaseSetup.FULL_SETUP).launch()
    }

    @Test
    fun fetchChannelsInGroup() {
        val group = groupRepository.getOne(1)
        val channels = group.channels
        assert(channels.size == 2)
        assert(channels.all { it.group == group })
    }

    @Test
    fun groupsForUser(){
        val user =  userRepository.findByEmail(email)
        val groups = user?.myGroups
        assert(groups!!.all { it.usersInGroup.contains(user) })
    }
}
