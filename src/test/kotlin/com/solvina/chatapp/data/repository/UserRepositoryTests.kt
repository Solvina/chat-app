package com.solvina.chatapp.data.repository

import com.ninja_squad.dbsetup.DbSetup
import com.ninja_squad.dbsetup.Operations
import com.ninja_squad.dbsetup.destination.DataSourceDestination
import com.solvina.chatapp.dbsetup.TestDatabaseSetup
import mu.KLogging
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import javax.sql.DataSource


/**
 * User: Vlastimil
 * Date: 12/6/17
 * Time: 2:43 PM
 *
 *
 */
@RunWith(SpringJUnit4ClassRunner::class)
@DataJpaTest
class UserRepositoryTests {
    companion object : KLogging()

    @Autowired
    lateinit var userRepository: UserRepository
    @Autowired
    @Qualifier("dataSource")
    lateinit var dataSource: DataSource

    val email = "test@test.com"
    val name = "John"
    @Before
    fun setUp() {
        val operation = Operations.sequenceOf(TestDatabaseSetup.DELETE_ALL
                , TestDatabaseSetup.INSERT_USER_ROLE)
        DbSetup(DataSourceDestination(dataSource), operation).launch()
    }

    @Test
    fun findUserByEmail() {
        val found = userRepository.findByEmail(email)!!
        assert(found.email.equals(email))
        logger.info("Found: ${found}")
    }

    @Test
    fun findUserByName() {
        val found = userRepository.findByName(name)
        assert(found.size == 2)
        logger.info("Found: ${found}")
    }
}