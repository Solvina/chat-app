package com.solvina.chatapp.web

import com.nhaarman.mockito_kotlin.doReturn
import com.nhaarman.mockito_kotlin.mock
import com.solvina.chatapp.data.model.Channel
import com.solvina.chatapp.data.model.Group
import com.solvina.chatapp.data.model.User
import com.solvina.chatapp.data.services.UserService
import mu.KLogging
import org.hamcrest.Matchers.containsString
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status




/**
 * User: Vlastimil
 * Date: 1/15/18
 * Time: 3:31 PM
 *
 *
 */
//@Ignore("Why all of a sudden user is not propagated to request?!")
@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringJUnit4ClassRunner::class)
class ChatControllerTests {
    companion object : KLogging()

    var EMAIL = "solvina@gmail.com"

    @Autowired
    lateinit var mockMvc: MockMvc
    @Autowired
    lateinit var chatController:ChatController

    @Test
    fun shouldRejectAccessToChat(){
        mockMvc.perform(get("/chat")).andExpect(status().is3xxRedirection)
    }
    @Test
    fun shouldAccessChatURL(){
        mockMvc.perform(get("/chat").with(user("solvina@gmail.com"))).andExpect(status().isOk)
//                .andDo(MockMvcResultHandlers.print())
                .andExpect(content().string(containsString("Chat for: solvina@gmail.com")))
    }

   @Test
    fun shouldDisplayLatestGroup(){
        val userServiceMock = mock<UserService>{
            on { findByEmail("solvina@gmail.com") } doReturn  createUser()
        }

       chatController.userService = userServiceMock
       
       mockMvc.perform(get("/channels").with(user("solvina@gmail.com"))).andExpect(status().isOk)
               .andExpect(content().string(containsString("Channels for: solvina@gmail.com")))
       
   }

    private fun createUser():User{
        var user = User(1,EMAIL)
        var group = Group(1,user)
        var channel = Channel(1, user, group)
        user.latestChannel = channel.id
        user.latestGroup = group.id

        return user
    }
}

