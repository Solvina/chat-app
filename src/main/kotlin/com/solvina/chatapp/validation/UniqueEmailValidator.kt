package com.solvina.chatapp.validation

import com.solvina.chatapp.data.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import javax.validation.ConstraintValidator
import javax.validation.ConstraintValidatorContext


/**
 * User: Vlastimil
 * Date: 1/31/18
 * Time: 9:37 AM
 *
 *
 */
class UniqueEmailValidator : ConstraintValidator<UniqueEmail, String> {
    @Autowired lateinit var userService: UserService


    override fun isValid(value: String, context: ConstraintValidatorContext?): Boolean {
        return userService.isEmailAvailable(value);
    }

    override fun initialize(constraintAnnotation: UniqueEmail?) {
    }

}