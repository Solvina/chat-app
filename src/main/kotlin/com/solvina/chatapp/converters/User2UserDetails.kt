package com.solvina.chatapp.converters

import com.solvina.chatapp.data.model.User
import com.solvina.chatapp.security.UserDetailImpl
import org.springframework.core.convert.converter.Converter
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.stereotype.Component


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 4:02 PM
 *
 *
 */
@Component
class User2UserDetails: Converter<User, UserDetails> {

    override fun convert(user: User): UserDetails {
        val auth: Collection<SimpleGrantedAuthority> = user.roles.map { SimpleGrantedAuthority(it.role) }
        return UserDetailImpl(auth.toMutableList(), user.email, user.password)
    }

}