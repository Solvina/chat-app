package com.solvina.chatapp.security

import com.solvina.chatapp.converters.User2UserDetails
import com.solvina.chatapp.data.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.core.userdetails.UserDetails
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.core.userdetails.UsernameNotFoundException
import org.springframework.stereotype.Service


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 4:28 PM
 *
 *
 */
@Service("userDetailsService")
class UserDetailServiceImpl(@Autowired val userService: UserService,
                            @Autowired val user2UserDetails: User2UserDetails) : UserDetailsService {

    @Throws(UsernameNotFoundException::class)
    override fun loadUserByUsername(email: String): UserDetails {
        var user = userService.findByEmail(email)
                ?: throw UsernameNotFoundException("Unable to find user with email: " + email)
        
        return user2UserDetails.convert(user)
    }
}