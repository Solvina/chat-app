package com.solvina.chatapp.security

import org.springframework.security.core.GrantedAuthority
import org.springframework.security.core.authority.SimpleGrantedAuthority
import org.springframework.security.core.userdetails.UserDetails


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 3:37 PM
 *
 *
 */
class UserDetailImpl(private val authorities: MutableList<SimpleGrantedAuthority> = mutableListOf(),
                     private val userName: String = "",
                     private val password: String = "") : UserDetails {


    override fun getAuthorities(): MutableCollection<out GrantedAuthority> = authorities

    override fun isEnabled(): Boolean = true

    override fun getUsername(): String = userName

    override fun isCredentialsNonExpired() = true

    override fun getPassword(): String = password

    override fun isAccountNonExpired(): Boolean = true
    override fun isAccountNonLocked(): Boolean = true
}