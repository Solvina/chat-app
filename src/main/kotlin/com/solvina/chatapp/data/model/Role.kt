package com.solvina.chatapp.data.model

import com.solvina.chatapp.converters.LocalDateTimeConverter
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 11:53 AM
 *
 *
 */

@Entity
@Table(name = "role")
@IdClass(RoleId::class)
data class Role(
        @Id
        @get: NotEmpty
        val email: String = "",
        @Id
        @get: NotEmpty
        val role: String = "",
        @Convert(converter = LocalDateTimeConverter::class)
        val stamp: LocalDateTime = LocalDateTime.now()


) : Serializable

internal data class RoleId(val email: String = "",
                           val role: String = "") : Serializable
