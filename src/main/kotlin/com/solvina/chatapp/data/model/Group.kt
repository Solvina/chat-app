package com.solvina.chatapp.data.model

import com.solvina.chatapp.converters.LocalDateTimeConverter
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 11:01 AM
 *
 *
 */
@Entity
@Table(name = "group_table")
data class Group(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "group_id")
        val id: Long = 0,

        @get: NotEmpty
        @ManyToOne
        @JoinColumn(name = "user_id")
        val owner: User = User(),

        @Convert(converter = LocalDateTimeConverter::class)
        val stamp: LocalDateTime = LocalDateTime.now()

) : Serializable {
    @get: NotEmpty
    val name: String = ""


    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY)
    val channels: Collection<Channel> = ArrayList()

    @ManyToMany
    @JoinTable(name = "user_group",
            joinColumns = arrayOf(JoinColumn(name = "group_id", referencedColumnName = "group_id")),
            inverseJoinColumns = arrayOf(JoinColumn(name = "user_id", referencedColumnName = "user_id")))
    val usersInGroup: Collection<User> = ArrayList()
}