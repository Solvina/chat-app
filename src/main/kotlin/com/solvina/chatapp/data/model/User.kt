package com.solvina.chatapp.data.model

import com.solvina.chatapp.converters.LocalDateTimeConverter
import com.solvina.chatapp.validation.UniqueEmail
import org.hibernate.annotations.Cascade
import org.hibernate.annotations.CascadeType
import javax.validation.constraints.Email
import javax.validation.constraints.NotBlank
import java.io.Serializable
import java.time.LocalDateTime
import java.util.*
import javax.persistence.*


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 11:15 AM
 *
 *
 */
@Entity
@Table(name = "user")
data class User(
		@field: Id
		@field: GeneratedValue(strategy = GenerationType.AUTO)
		@field: Column(name = "user_id", updatable = false, nullable = false)
		val id: Long = 0,

		@field: NotBlank
		@field: Email
		@field: UniqueEmail
		var email: String = "",

		@Convert(converter = LocalDateTimeConverter::class)
		val stamp: LocalDateTime = LocalDateTime.now()


) : Serializable {

	var latestGroup: Long? = null

	var latestChannel: Long? = null
	@get: NotBlank
	var password: String = ""

	@get: NotBlank
	var name: String = ""

	var note: String? = ""

	@OneToMany(fetch = FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	@JoinColumn(name = "user_id", referencedColumnName = "user_id")
	var roles: Collection<Role> = ArrayList()

	@OneToMany(mappedBy = "author", fetch = FetchType.LAZY)
	var articles: Collection<Article> = listOf()

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "owner")
	var ownedChannels: Collection<Channel> = ArrayList()

	@ManyToMany(fetch = FetchType.LAZY, mappedBy = "usersInGroup")
	var myGroups: Collection<Group> = ArrayList()

	@ManyToMany(fetch = FetchType.LAZY)
	@JoinTable(name = "user_channel",
			joinColumns = arrayOf(JoinColumn(name = "user_id", referencedColumnName = "user_id")),
			inverseJoinColumns = arrayOf(JoinColumn(name = "channel_id", referencedColumnName = "channel_id")))
	var channels: Collection<Channel> = ArrayList()


}
