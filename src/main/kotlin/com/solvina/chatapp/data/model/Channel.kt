package com.solvina.chatapp.data.model

import com.solvina.chatapp.converters.LocalDateTimeConverter
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 11:01 AM
 *
 *
 */
@Entity
@Table(name = "channel")
data class Channel(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "channel_id")
        val id: Long = 0,

        @get: NotEmpty
        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "user_id")
        val owner: User,

        @ManyToOne(fetch = FetchType.LAZY)
        @JoinColumn(name = "group_id")
        val group: Group,
        
        @Convert(converter = LocalDateTimeConverter::class)
        val stamp: LocalDateTime = LocalDateTime.now()


) : Serializable {


    @ManyToMany(mappedBy = "channels")
    val users: Collection<User> = ArrayList()


    @get:NotEmpty
    val name: String = ""

    @OneToMany(mappedBy = "channel", fetch = FetchType.LAZY)
    val articles: Collection<Article> = ArrayList()

}