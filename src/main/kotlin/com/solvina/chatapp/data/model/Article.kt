package com.solvina.chatapp.data.model

import com.solvina.chatapp.converters.LocalDateTimeConverter
import java.io.Serializable
import java.time.LocalDateTime
import javax.persistence.*
import javax.validation.constraints.NotEmpty


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 11:03 AM
 *
 *
 */
@Entity
@Table(name = "article")
data class Article(
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        @Column(name = "article_id")
        val id: Long = 0,
        

        @ManyToOne
        @JoinColumn(name = "user_id")
        val author: User,

        @ManyToOne
        @JoinColumn(name = "channel_id")
        val channel: Channel,

        @get: NotEmpty
        @Column(columnDefinition = "text")
        val text: String = "",
        
        @Convert(converter = LocalDateTimeConverter::class)
        val stamp: LocalDateTime = LocalDateTime.now()


) : Serializable 