package com.solvina.chatapp.data.services.impl

import com.solvina.chatapp.data.model.Role
import com.solvina.chatapp.data.repository.RoleRepository
import com.solvina.chatapp.data.services.RoleService
import org.springframework.stereotype.Service
import javax.transaction.Transactional


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 3:17 PM
 *
 *
 */
@Service
class RoleServiceImpl (val roleRepository: RoleRepository): RoleService {
    override fun findAll(): Collection<Role> = roleRepository.findAll()

    override fun findById(id: Long): Role = roleRepository.getOne(id)

    override fun saveOrUpdate(item: Role): Role = roleRepository.save(item)

    @Transactional
    override fun delete(item: Role) = roleRepository.delete(item)
}