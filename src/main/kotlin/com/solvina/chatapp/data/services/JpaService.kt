package com.solvina.chatapp.data.services


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 11:57 AM
 *
 *
 */
interface JpaService<T, ID> {
    fun findAll(): Collection<T>
    fun findById(id: ID): T
    fun saveOrUpdate(item: T): T
    fun delete(item: T)
}