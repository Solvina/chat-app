package com.solvina.chatapp.data.services

import com.solvina.chatapp.data.model.Article


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 12:23 PM
 *
 *
 */
interface ArticleService : JpaService<Article, Long>