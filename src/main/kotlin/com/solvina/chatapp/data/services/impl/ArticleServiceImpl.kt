package com.solvina.chatapp.data.services.impl

import com.solvina.chatapp.data.model.Article
import com.solvina.chatapp.data.repository.ArticleRepository
import com.solvina.chatapp.data.services.ArticleService
import org.springframework.stereotype.Service


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 12:24 PM
 *
 *
 */
@Service
class ArticleServiceImpl(val repository: ArticleRepository) : ArticleService {
    override fun findAll(): Collection<Article> = repository.findAll()

    override fun findById(id: Long): Article = repository.getOne(id)

    override fun saveOrUpdate(item: Article): Article = repository.save(item)

    override fun delete(item: Article) = repository.delete(item)

}