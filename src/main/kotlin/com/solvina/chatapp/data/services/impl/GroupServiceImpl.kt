package com.solvina.chatapp.data.services.impl

import com.solvina.chatapp.data.model.Group
import com.solvina.chatapp.data.repository.GroupRepository
import com.solvina.chatapp.data.services.GroupService
import org.springframework.stereotype.Service


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 12:24 PM
 *
 *
 */
@Service
class GroupServiceImpl(val groupRepository: GroupRepository) : GroupService {
    override fun findAll(): Collection<Group> = groupRepository.findAll()

    override fun findById(id: Long): Group = groupRepository.getOne(id)

    override fun saveOrUpdate(item: Group): Group = groupRepository.save(item)

    override fun delete(item: Group) = groupRepository.delete(item)

}