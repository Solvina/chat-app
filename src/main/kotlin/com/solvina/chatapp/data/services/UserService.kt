package com.solvina.chatapp.data.services

import com.solvina.chatapp.data.model.User


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 12:03 PM
 *
 *
 */
interface UserService : JpaService<User, Long> {


    fun findByName(name: String): Collection<User>

    fun findByEmail(email: String): User?

    fun isEmailAvailable(email:String): Boolean
}