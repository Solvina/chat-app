package com.solvina.chatapp.data.services.impl

import com.solvina.chatapp.data.model.Channel
import com.solvina.chatapp.data.repository.ChannelRepository
import com.solvina.chatapp.data.services.ChannelService
import org.springframework.stereotype.Service


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 12:24 PM
 *
 *
 */
@Service
class ChannelServiceImpl(val channelRepository: ChannelRepository) : ChannelService {
    override fun findAll(): Collection<Channel> = channelRepository.findAll()

    override fun findById(id: Long): Channel = channelRepository.getOne(id)

    override fun saveOrUpdate(item: Channel): Channel = channelRepository.save(item)

    override fun delete(item: Channel) = channelRepository.delete(item)

}