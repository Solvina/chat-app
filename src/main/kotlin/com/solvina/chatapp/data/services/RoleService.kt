package com.solvina.chatapp.data.services

import com.solvina.chatapp.data.model.Role


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 12:03 PM
 *
 *
 */
interface RoleService : JpaService<Role, Long>