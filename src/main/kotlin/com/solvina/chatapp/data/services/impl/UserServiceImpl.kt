package com.solvina.chatapp.data.services.impl

import com.solvina.chatapp.data.model.User
import com.solvina.chatapp.data.repository.UserRepository
import com.solvina.chatapp.data.services.UserService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.security.crypto.password.PasswordEncoder
import org.springframework.stereotype.Service
import javax.transaction.Transactional


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 12:04 PM
 *
 *
 */
@Service("userService")
class UserServiceImpl(@Autowired var userRepository: UserRepository,
                      @Autowired var passwordEncoder: PasswordEncoder) : UserService {

    override fun findByName(name: String): Collection<User> = userRepository.findByName(name)

    override fun findByEmail(email: String): User? = userRepository.findByEmail(email)

    override fun isEmailAvailable(email: String): Boolean = findByEmail(email) == null

    override fun findAll(): Collection<User> = userRepository.findAll()

    override fun findById(id: Long) = userRepository.getOne(id)

    override fun saveOrUpdate(item: User): User {
        if (item.id < 1)
            updateNewUser(item)

        return userRepository.save(item)
    }

    private fun updateNewUser(user: User): User {
        user.password = passwordEncoder.encode(user.password)

        return user
    }


    @Transactional
    override fun delete(item: User) = userRepository.delete(item)

}