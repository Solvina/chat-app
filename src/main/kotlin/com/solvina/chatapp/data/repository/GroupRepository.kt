package com.solvina.chatapp.data.repository

import com.solvina.chatapp.data.model.Group
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


/**
 * User: Vlastimil
 * Date: 12/11/17
 * Time: 11:38 AM
 *
 *
 */
@Repository
interface GroupRepository : JpaRepository<Group, Long> {
}