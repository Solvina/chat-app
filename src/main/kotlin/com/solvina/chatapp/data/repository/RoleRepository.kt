package com.solvina.chatapp.data.repository

import com.solvina.chatapp.data.model.Role
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 11:26 AM
 *
 *
 */
@Repository
interface RoleRepository : JpaRepository<Role, Long>