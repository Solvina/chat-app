package com.solvina.chatapp.data.repository

import com.solvina.chatapp.data.model.User
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository


/**
 * User: Vlastimil
 * Date: 12/5/17
 * Time: 11:26 AM
 *
 *
 */
@Repository
interface UserRepository : JpaRepository<User, Long> {

    fun findByName(name: String): Collection<User>
    
    fun findByEmail(email: String): User?
}