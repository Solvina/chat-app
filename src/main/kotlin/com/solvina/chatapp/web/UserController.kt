package com.solvina.chatapp.web

import com.solvina.chatapp.data.model.User
import com.solvina.chatapp.data.services.UserService
import mu.KLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Controller
import org.springframework.ui.ModelMap
import org.springframework.validation.BindingResult
import org.springframework.web.bind.annotation.ModelAttribute
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import javax.validation.Valid


@Controller
class UserController {
    companion object : KLogging()

    @Autowired
    lateinit var userService: UserService

    @ModelAttribute("user")
    fun newUser(): User = User()

    @RequestMapping("/addNewUser", method = [RequestMethod.POST])
    fun addNewUser(@Valid @ModelAttribute("user") user: User, validationResult: BindingResult, model: ModelMap): String {
        if(validationResult.hasErrors()){
            logger.error("We saw errors: {}", validationResult.errorCount)
            return "userForm"
        }

        logger.info(user.toString())
        userService.saveOrUpdate(user)
        return "viewUser"
    }

}

