package com.solvina.chatapp.web

import com.solvina.chatapp.data.services.UserService
import mu.KLogging
import org.springframework.security.core.context.SecurityContextHolder
import org.springframework.security.core.userdetails.User
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping


/**
 * User: Vlastimil
 * Date: 1/15/18
 * Time: 3:33 PM
 *
 *
 */
@Controller
class ChatController {
    companion object : KLogging()

    lateinit var userService:UserService

    private fun addUserToModel(model: Model, login: String){
        model.addAttribute("login", login)
    }
    fun getUser():User{
        val ret:User =  SecurityContextHolder.getContext().authentication.principal as User
        logger.info("User: "+ ret.toString())
        return  ret
    }

    @RequestMapping("/chat")
    fun chat(model: Model): String {
        addUserToModel(model,getUser().username)
        return "chat"
    }

    @RequestMapping("/channels")
    fun channels(model: Model): String {
        addUserToModel(model,getUser().username)
        return "channels"
    }

}