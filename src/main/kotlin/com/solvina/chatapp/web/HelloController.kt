package com.solvina.chatapp.web

import mu.KLogging
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping
import java.util.concurrent.atomic.AtomicInteger


/**
 * User: Vlastimil
 * Date: 12/4/17
 * Time: 9:20 AM
 *
 *
 */
val visits:AtomicInteger = AtomicInteger(0)

@Controller
class HelloController{

    companion object : KLogging()


    @RequestMapping("/")
    fun index(): String = "home"
}