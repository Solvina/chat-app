package com.solvina.chatapp

import mu.KLogging
import mu.KotlinLogging
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Bean
import java.util.*

@SpringBootApplication
class ChatAppApplication {

    companion object : KLogging()

    @Bean
    fun commandLineRunner(ctx: ApplicationContext): CommandLineRunner = CommandLineRunner {
        logger.info("Let's inspect the beans provided by Spring Boot:")
        val beanNames: Array<String> = ctx.beanDefinitionNames
        Arrays.sort(beanNames)

        for (beanName in beanNames)
            logger.info(beanName)
    }

}

fun main(args: Array<String>) {
    SpringApplication.run(ChatAppApplication::class.java, *args)
    KotlinLogging.logger { "We are up!" }
}

