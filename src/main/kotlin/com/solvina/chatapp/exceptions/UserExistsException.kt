package com.solvina.chatapp.exceptions


/**
 * User: Vlastimil
 * Date: 1/30/18
 * Time: 4:12 PM
 *
 *
 */
class UserExistsException(val msg:String): Exception(msg)