package com.solvina.chatapp.config

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.security.authentication.dao.DaoAuthenticationProvider
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter
import org.springframework.security.core.userdetails.UserDetailsService
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder
import org.springframework.security.crypto.password.PasswordEncoder


/**
 * User: Vlastimil
 * Date: 12/4/17
 * Time: 10:48 AM
 *
 *
 */
@Configuration
@EnableWebSecurity
class WebSecurityConfig : WebSecurityConfigurerAdapter() {

    override fun configure(http: HttpSecurity) {
        http
                .authorizeRequests()
                .antMatchers("/", "/home", "/addNewUser").permitAll()
                .anyRequest().authenticated()
                .and()
                .formLogin().loginPage("/login").permitAll()
                .and()
                .logout().permitAll()
    }

    @Bean
    fun daoAuthenticationProvider(passwordEncoder: PasswordEncoder,
                                  userDetailsService: UserDetailsService): DaoAuthenticationProvider {
        val ret = DaoAuthenticationProvider()
        ret.setUserDetailsService(userDetailsService)
        ret.setPasswordEncoder(passwordEncoder)

        return ret
    }


    @Autowired
    @Throws(Exception::class)
    fun configureGlobal(auth: AuthenticationManagerBuilder) {
        auth.authenticationProvider(daoAuthenticationProvider())
    }


    @Bean
    @Qualifier("passwordEncoder")
    fun passwordEncoder(): PasswordEncoder = TestPasswordEncoder()

    @Bean
    fun daoAuthenticationProvider(): DaoAuthenticationProvider {
        val ret = DaoAuthenticationProvider()
        ret.setPasswordEncoder(passwordEncoder())
        ret.setUserDetailsService(userDetailsService())
        return ret
    }

    @Autowired
    fun configureAuthManager(authenticationManagerBuilder: AuthenticationManagerBuilder) {
        authenticationManagerBuilder.authenticationProvider(daoAuthenticationProvider())
    }
    
}

class TestPasswordEncoder:PasswordEncoder{
    override fun encode(rawPassword: CharSequence?): String = "notEncoded"

    override fun matches(rawPassword: CharSequence?, encodedPassword: String?): Boolean = true

}